drop table sports.rivalry;

create view sports.rivalry
as
SELECT    players.*,
		  home.total_home_games,
          home.home_win,
          home.home_loss,
          home.home_draw,
          away.total_away_games,
          away.away_win,
          away.away_loss,
          away.away_draw,
          #away.total_away_games + home.total_home_games          AS total_games,
          (CASE WHEN home.total_home_games is null THEN 0 ELSE home.total_home_games END) + (CASE WHEN away.total_away_games is null THEN 0 ELSE away.total_away_games END) AS total_games,

		  #home.home_win        + away.away_win                                             AS total_wins,
          (CASE WHEN home.home_win is null THEN 0 ELSE home.home_win END) + (CASE WHEN away.away_win is null THEN 0 ELSE away.away_win END)  AS total_wins,
          
          #home.home_loss        + away.away_loss                                              AS total_losses,
          (CASE WHEN home.home_loss is null THEN 0 ELSE home.home_loss END) + (CASE WHEN away.away_loss is null THEN 0 ELSE away.away_loss END)  AS total_losses,
          
          #(home.home_win  + away.away_win) / (home.total_home_games + away.total_away_games)  AS win_ratio,
           ((CASE WHEN home.home_win is null THEN 0 ELSE home.home_win END) + (CASE WHEN away.away_win is null THEN 0 ELSE away.away_win END)) / ((CASE WHEN home.total_home_games is null THEN 0 ELSE home.total_home_games END) + (CASE WHEN away.total_away_games is null THEN 0 ELSE away.total_away_games END)) AS win_ratio,
          
          #(home.home_loss + away.away_loss) / (home.total_home_games + away.total_away_games) AS loss_ratio
          ((CASE WHEN home.home_loss is null THEN 0 ELSE home.home_loss END) + (CASE WHEN away.away_loss is null THEN 0 ELSE away.away_loss END)) / ((CASE WHEN home.total_home_games is null THEN 0 ELSE home.total_home_games END) + (CASE WHEN away.total_away_games is null THEN 0 ELSE away.total_away_games END)) AS loss_ratio

from 
(
  select distinct player_one_id, 
    player_two_id
  FROM sports.match 
  UNION
  select distinct player_two_id, player_one_id 
  from sports.match 
  order by player_one_id
) as players
left join
(
  SELECT player_one_id, 
    player_two_id, 
    count(*) total_home_games, 
    SUM(CASE WHEN winner= 1 THEN 1 ELSE 0 END) as home_win,
    SUM(CASE WHEN winner= 2 THEN 1 ELSE 0 END) as home_loss,
    SUM(CASE WHEN winner is null THEN 1 ELSE 0 END) as home_draw
  FROM sports.match 
  group by player_one_id, player_two_id
  order by player_one_id
) as home
on players.player_one_id = home.player_one_id
and players.player_two_id = home.player_two_id
left join
(
  SELECT player_one_id, 
    player_two_id, 
    count(*) total_away_games,
    SUM(CASE WHEN winner= 2 THEN 1 ELSE 0 END) as away_win,
    SUM(CASE WHEN winner= 1 THEN 1 ELSE 0 END) as away_loss,
    SUM(CASE WHEN winner is null THEN 1 ELSE 0 END) as away_draw
  FROM sports.match
  group by player_one_id, player_two_id
  order by player_one_id
) as away
on players.player_one_id = away.player_two_id
and players.player_two_id = away.player_one_id
order by players.player_one_id;

INSERT INTO sports.user VALUES (1, "admin@admin.com", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f", 2,0,0);
INSERT INTO sports.user VALUES (2, "director@director.com", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f", 1,0,0);
INSERT INTO sports.user VALUES (3, "medvedev@standard.com", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f", 0,0,0);

INSERT INTO sports.tournament VALUES ("1", "FirstTournament", "knockout", "TL", "GoldenMedal", 2, 0);

INSERT INTO sports.player_profile VALUES (1,"Daniil Medvedev","Argentina",0,0,0,0,null,null,null,null,null,null,1,"1");
INSERT INTO sports.player_profile VALUES (2,"Rafael Nadal","Portugal",0,0,0,0,null,null,null,null,null,null,null,"1");
INSERT INTO sports.player_profile VALUES (3,"Djokovic","Macedonia",0,0,0,0,null,null,null,null,null,null,null,"1");
INSERT INTO sports.player_profile VALUES (4,"Grigor Dimitrov","Bulgaria",0,0,0,0,null,null,null,null,null,null,null,"1");

INSERT INTO sports.match VALUES (1,"2022-11-17",null,null,null,null,"1",1,2,"TL");
INSERT INTO sports.match VALUES (2,"2022-11-17",null,null,null,null,"1",1,2,"TL");








