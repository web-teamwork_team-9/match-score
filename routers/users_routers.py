from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi import APIRouter, Security, Depends
from common.responses import BadRequest, NoContent
from common.database import *
from data.data_models import LoginData
from services import users_services

users_router = APIRouter(prefix='/users')


@users_router.post('/')
def login(data: LoginData, session: Session = Depends(get_session)):
    """
    Gives the user a token if the login is succesfull

    Args:
        data (LoginData): Input data (username and password)

    Returns:
        Returns the Token
    """
    user = users_services.try_login(data.email, data.password, session)
    # why not in the services layer?
    if user:
        token = users_services.create_token(user.email, session)
        return {"token" : token}
    else:
        return BadRequest('Invalid login data')


@users_router.post('/register')
def register(data: LoginData, session: Session = Depends(get_session)):
    """
    Registers a new user

    Args:
        data (RegistrationData): Object of all the input data required when registering

    Returns:
        Returns the new User object or BadRequest
    """
    user = users_services.register(data.email, data.password, session)
    return user


@users_router.post('/director')
def post_items(head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return users_services.post_promotion(head_token.credentials, session)


@users_router.get('/director')
def get_items(head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return users_services.get_promotions(head_token.credentials, session)


@users_router.post('/director/{email}')
def upgrade(email: str, action: str | None = None, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return users_services.req_for_director(email, action, head_token.credentials, session)


@users_router.post('/profile/{id}')
def post_profile(id: int, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return users_services.post_profile(id, head_token.credentials, session)


@users_router.get('/profile')
def get_profiles(head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return users_services.get_profiles(head_token.credentials, session)


@users_router.put('/profile/{email}')
def connect_profile(email: str, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return users_services.connect_profile(email, head_token.credentials, session)
