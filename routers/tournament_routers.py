from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi import APIRouter, Security, Depends
from data.data_models import tournament_creation
from services import tournament_services
from common.database import *

tournaments_router = APIRouter(prefix='/tournaments')


@tournaments_router.post('/')
def add_tournament(event: tournament_creation, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return tournament_services.create_tournament(event, head_token.credentials, session)


@tournaments_router.get('/all/')
def get_matches(session: Session = Depends(get_session)):
    return tournament_services.get_all(session)


@tournaments_router.get('/{name}')
def get_event_id(name: str, session: Session = Depends(get_session)):
    return tournament_services.get_event_name(name, session)
