from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi import APIRouter, Security, Depends
from data.data_models import player_profile, profile_updates
from services import players_services
from common.database import *

players_router = APIRouter(prefix='/players')


@players_router.post('/')
def add_new_profile(profile: player_profile, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return players_services.create_new_profile(profile, head_token.credentials, session)


@players_router.post('/{id}')
def update_profile(id: int, profile: profile_updates, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return players_services.update_profile(id, profile, head_token.credentials, session)


@players_router.get('/{id}')
def view_profile(id: int, session: Session = Depends(get_session)):
    return players_services.view_profile(id, session)
