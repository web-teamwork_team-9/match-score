from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi import APIRouter, Security, Depends
from common.responses import BadRequest
from common.database import *
from data.data_models import *
from services import admin_services

admin_router = APIRouter(prefix='/admin')


@admin_router.delete('/user/{email}')
def delete_user(email: str, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return admin_services.delete_user(email, head_token.credentials, session)


@admin_router.delete('/player/{id}')
def delete_player(id: int, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return admin_services.delete_player(id, head_token.credentials, session)


@admin_router.delete('/match/{id}')
def delete_match(id: int, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return admin_services.delete_match(id, head_token.credentials, session)


@admin_router.delete('/tournament/{title}')
def delete_tournament(title: str, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    if title.isnumeric(): return BadRequest("Bad data.")
    return admin_services.delete_tournament(title, head_token.credentials, session)
