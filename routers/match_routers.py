from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi import APIRouter, Security, Depends
from data.data_models import match, match_score
from services import match_services
from common.database import *

match_router = APIRouter(prefix='/match')


@match_router.post('/')
def add_match(event: match, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return match_services.create_match(event, head_token.credentials, session)


@match_router.get('/{id}')
def get_event_id(id: int, session: Session = Depends(get_session)):
    return match_services.get_event_id(id, session)


@match_router.get('/all/')
def get_matches(session: Session = Depends(get_session)):
    return match_services.get_all(session)


@match_router.put('/{id}')
def update_match(id: int, event: match_score, head_token: HTTPAuthorizationCredentials = Security(HTTPBearer()), session: Session = Depends(get_session)):
    return match_services.update_normal_match(id, event, head_token.credentials, session)
