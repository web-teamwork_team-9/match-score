from services.users_services import _is_authenticated, _check_role
from services.match_services import create_tournament_matches
from common.responses import BadRequest, Unauthorized, NoContent
from common.database import query_select, query_upsert, query_delete, query_select_multiple, query_select_no_column
from common.mail_notif import send_mail_add_match
from data.data_models import tournament_creation, tournament, player_profile, user
from itertools import combinations
from sqlalchemy import inspect
import uuid

allowed_chars= set(("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_- "))


def create_tournament(event: tournament_creation, token: str, session) -> Unauthorized | BadRequest | NoContent:
    """Creates a new Tournament
    1. Checks the bearer token
    2. Checks if the name is valid
    3. Checks if there is already Tournament with same name in the DB
    4. Checks if the user is authorized to create Tournaments
    5. Checks if format is correct
    6. Checks if match_format is correct
    7. Checks if participants are the correct number from a knockout or league tourn.
    8. Creates a new tourn. object with the given data
    9. Uploads it to the DB
    10. _check participant profiles func deals with creating new player profiles if they don't exist already and sets their cur_tournament
    11. Uploads the profiles to the DB


    Args:
        event (tournament_creation): Tournament_creation object with the new tourn. data
        token (str): Bearer Token
        session (_type_): ORM Session

    Returns:
        Unauthorized | BadRequest | NoContent
    """
    id = _is_authenticated(token)

    if len(event.title) < 5:
        return BadRequest("Tournament title must be at least 5 characters.")

    if query_select(tournament, tournament.title, event.title, session):
        return BadRequest(f"There is already a tournament named '{event.title}'")

    if _check_role(id, session) not in [1, 2]:
        return Unauthorized("You need to be an admin or director.")

    if event.format.lower() not in ["knockout", "league"]:
        return BadRequest("Invalid tournament format.")

    if event.match_format not in ["TL", "SL"]:
        return BadRequest("Invalid match format. Must be 'TL' or 'SL'.")

    if event.format.lower() == "knockout":
        if len(event.participants) not in [2,4,8,16,32,64,128,256,512,1024,2048,5096,10192]: return BadRequest("Invalid number of players for knockout tournament.")
        else: matches_to_be_played = int(len(event.participants) / 2)

    elif event.format.lower() == "league":
        if len(event.participants) not in range(3, 51): return BadRequest("Invalid number of players for league tournament.")
        else: matches_to_be_played = int(len(list(combinations(event.participants, 2))))

    try:
        tour_id = str(uuid.uuid4())
        new_tour = tournament(title = event.title, format = event.format, match_format = event.match_format,
        prize = event.prize, tournament_id = tour_id, matches_to_play = matches_to_be_played)
        query_upsert(new_tour, session)
        profiles = check_participants_profiles(event.participants, tour_id, session)
        if type(profiles) is not list:
            query_delete(new_tour, session)
            return profiles
        query_upsert(profiles, session)
        query_upsert(create_tournament_matches(event.participants, new_tour, session), session)
        return NoContent()
    except:
        return BadRequest("Ooops, something went wrong...")


def get_all(session) -> list:
    """ Finds all tournaments in the DB
    1. Selects all tournaments from the DB
    2. Itirates through every tournament and orders their attributes

    Args:
        session (_type_): ORM Session

    Returns:
        list or ordered tournament objects
    """
    data = query_select_no_column(tournament, session)
    data_ordered = []
    for tourn in data:
        new = {key: getattr(tourn, key)
            for key in inspect(tourn).mapper.column_attrs.keys()}
        data_ordered.append(new)

    return data_ordered if data_ordered else BadRequest("No tournaments found.")


def get_event_name(name: str, session) -> dict:
    """Find a tournament by name
    1. Selects it the tournament from the DB
    2. Orders it's attributes

    Args:
        name (str): _description_
        session (_type_): _description_

    Returns:
        dict: _description_
    """
    data = query_select(tournament, tournament.title, name, session)
    return {key: getattr(data, key)
            for key in inspect(data).mapper.column_attrs.keys()} if data else BadRequest("No such tournament.")


def check_participants_profiles(participants: list[str], tour_id: str, session) -> list:
    """Used in the create_tournament func to check if player names already have profiles and creates new ones for the new names
    1. Selects all existing player profiles
    2. Creates a new 'names' list for every existing player_profile
    3. Itirates through each name in participans which is not an existing profile
    4. Checks the name with _name_validation, creates a new player_profile object and adds it to 'profiles'
    5. Itirates through each name in data to set their cur_tourn ID to this tournament

    Args:
        participants (list[str]): List of names
        tour_id (str): Tournament ID
        session (_type_): ORM Session

    Returns:
        list of player_profile objects
    """
    data = query_select_multiple(player_profile, player_profile.name, participants, session)
    names = [player.name for player in data]
    profiles = []

    for each in data:
        if each.user_id not in [0, None]:
            user_data = query_select(user, user.user_id, each.user_id, session)
            send_mail_add_match(user_data.email)

    for one in participants:
        if one not in names:
            prof = _name_validation(one, tour_id)
            if not prof:
                return BadRequest("Invalid player name.")
            profiles.append(prof)

    for player in data:
        if player.current_tournament in [0, "0", None, tour_id]:
            player.current_tournament = tour_id
            profiles.append(player)
        else:
            return BadRequest(f"{player.name} is already in another tournament.")

    return profiles


def _name_validation(name: str, tour_id: str) -> player_profile:
    """Validates new profiles
    1. Checks if name contains only allowed characters
    2. Creates a new player profile with the given name and current_torunament ID

    Args:
        name (str): Name
        tour_id (str): Tournament ID

    Returns:
        player_profile object
    """
    validation = set((name))

    if not validation.issubset(allowed_chars) or len(name) < 2: return False
    else: return player_profile(name = name, country = "N/A", current_tournament = tour_id)

