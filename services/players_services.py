from common.responses import Unauthorized, NoContent, BadRequest
from common.database import *
from data.data_models import *
from services.users_services import _is_authenticated, _check_role
from sqlalchemy import inspect


def create_new_profile(profile: player_profile, token: str, session) -> BadRequest | Unauthorized | NoContent:
    """Creates a new player profile
    1. Checks if the role of the bearer token
    2. Checks the DB if a player with such name already exists
    3. Uploads the new profile to the DB

    Args:
        profile (player_profile): Profile info object
        token (str): Bearer token
        session (_type_): ORM Session

    Returns:
        BadRequest | Unauthorized | NoContent
    """
    try:
        id = _is_authenticated(token)
        if _check_role(id, session) not in [1, 2]:
            return Unauthorized("You need to be an admin or director.")
        if query_select(player_profile, player_profile.name, profile.name, session):
            return BadRequest(f'Player with name "{profile.name}" already exists.')
        query_upsert(profile, session)
        return NoContent()
    except:
        return BadRequest("Ooops, something went wrong...")


def update_profile(profile_id: int , profile: profile_updates, token: str, session) -> BadRequest | Unauthorized | NoContent:
    """ Updates an existing player profile
    1. Checks the role of the bearer token
    2. Selects the profile from the DB
    3. Checks if such a player exists
    4. Checks if the token is either an admin, director or a normal user connected to the profile
    5. Updates the selected profile
    6. Uploads the updated profile to the DB

    Args:
        profile_id (int): profile ID
        profile (profile_updates): Updates object
        token (str): Bearer token
        session (_type_): ORM Session

    Returns:
        BadRequest | Unauthorized | NoContent
    """
    try:
        user_id = _is_authenticated(token)

        player = query_select(player_profile, player_profile.profile_id, profile_id, session)

        if not player:
            return BadRequest("No such player.")

        if (player.user_id == user_id) or (player.user_id in [None, 0] and _check_role(user_id, session) != 0) or (_check_role(user_id, session) == 2):
            pass
        else:
            return Unauthorized("You do not have the required permission to perform this action.")

        if profile.name != None:
            player.name = profile.name
        if profile.country != None:
            player.country = profile.country
        if profile.club_name != None:
            player.club_name = profile.club_name
        if profile.user_id != None:
            player.user_id = profile.user_id

        query_upsert(player, session)
        return NoContent()
    except:
        return BadRequest("Ooops, something went wrong...")


def view_profile(id: int, session) -> dict | BadRequest:
    """Find a profile in the DB
    1. Selects a profile with give ID from the DB
    2. If there is no such id the func will return BadRequest
    3. If there is such a player we use a dict compr. to order all the attributes (otherwise they'd be given at random) and return it

    Args:
        id (int): Profile ID
        session (_type_): ORM Session

    Returns:
        dict | BadRequest: _description_
    """
    profile = query_select(player_profile, player_profile.profile_id, id, session)

    if profile:
        return {key: getattr(profile, key)
                for key in inspect(profile).mapper.column_attrs.keys()}
    return BadRequest("No such player.")


def _check_max_win_rate(id: int, session) -> None | rivalry:
    """ Finds the record where the given player has the highest win rate against every opponent he's ever played
    1. Selects all the records from the DB with the player ID
    2. Itirates over all of them and compares them
    3. Returns the record object with the highes win rate

    Args:
        id (int): ID of the player
        session (_type_): ORM Session

    Returns:
        rivalry object
    """
    data = queries_select(rivalry, rivalry.player_one_id, id, session)
    current_max = 0
    current_max_object = None
    for param in data:
        if current_max_object == None:
            current_max_object = param
        if param.win_ratio >= current_max:
            current_max = param.win_ratio
            current_max_object = param
    return current_max_object


def _check_min_win_rate(id: int, session) -> None | rivalry:
    """ Finds the record where the given player has the lowest win rate against every opponent he's ever played
    1. Selects all the records from the DB with the player ID
    2. Itirates over all of them and compares them
    3. Returns the record object with the lowest win rate

    Args:
        id (int): ID of the player
        session (_type_): ORM Session

    Returns:
        rivalry object
    """
    data = queries_select(rivalry, rivalry.player_one_id, id, session)
    current_min = 1
    current_min_object = None
    for param in data:
        if current_min_object == None:
            current_min_object = param
        this_ratio = param.win_ratio
        if this_ratio <= current_min:
            current_min = this_ratio
            current_min_object = param
    return current_min_object


def select_most_oft_opponent(player_id: int, session) -> None | rivalry:
    """ Checks who's opponent with most games against the given player ID
    1. Selects all the records with the give player ID
    2. Itirates over all of them to the find the one where the total_games are most
    3. Returns the rivalry object with the highest total_games

    Args:
        player_id (int): Player ID
        session (_type_): ORM Session

    Returns:
        rivalry object
    """
    query = queries_select(rivalry, rivalry.player_one_id, player_id, session)
    current_games = 0
    current_game_object = None
    for param in query:
        games = param.total_games
        if games == None:
            games = 0
        if games >= current_games:
            current_games = games
            param.total_games = current_games
            current_game_object = param
    return current_game_object


def _update_profile_ratio_statistics(match: match, session) -> None | BadRequest:
    """ This function is called after every match update
    1. Selects the player_profile objects from the DB (the match participants)
    2. Updates the objects for each player by finding the data with the _check_min/_check_max/select_most
    3. Updates the tourn statistics for each player by finding the data from the profile_tournament_records in the DB
    4. Uploads the updated objects to the DB

    Args:
        match (match): Match object
        session (_type_): ORM Session

    Returns:
        None | BadRequest
    """
    try:
        player1 = query_select(player_profile, player_profile.profile_id, match.player_one_id, session)
        player2 = query_select(player_profile, player_profile.profile_id, match.player_two_id, session)

        if match.winner == 1:
            player1.matches_won += 1
        if match.winner == 2:
            player2.matches_won += 1

        for player in [player1, player2]:
            player.matches_played += 1

            winner_win_ratio_data = _check_max_win_rate(player.profile_id, session)
            player.best_opponent = winner_win_ratio_data.player_two_id
            player.best_opp_w_l_ratio = winner_win_ratio_data.win_ratio

            player_lose_ratio_data = _check_min_win_rate(player.profile_id, session)
            player.worst_opponent = player_lose_ratio_data.player_two_id
            player.worst_opp_w_l_ratio = player_lose_ratio_data.win_ratio

            the_object = select_most_oft_opponent(player.profile_id, session)
            player.most_often_opponent = the_object.player_two_id

            player.tournaments_played = len(queries_select(profile_tournament_records, profile_tournament_records.player_id, player.profile_id, session))
            player.tournaments_won = len(queries_select_two_params(profile_tournament_records, profile_tournament_records.player_id, player.profile_id, profile_tournament_records.is_winner, 1, session))

        query_upsert([player1,player2], session)
    except: 
        return BadRequest("Ooops, something went wrong...")
