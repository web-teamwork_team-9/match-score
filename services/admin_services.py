from common.responses import Unauthorized, BadRequest, NoContent
from common.database import *
from data.data_models import *
from services.users_services import _is_authenticated, _check_role


def delete_user(email: str, token: str, session) -> Unauthorized | BadRequest | NoContent:
    """Deletes a user

    Args:
        email (str): email of the user to be deleted
        token (str): Bearer token
        session (_type_): ORM Session

    Returns:
        Unauthorized if you're not an admin | BadRequest if the user is not found| NoContent: If successful
    """
    try:
        id = _is_authenticated(token)

        if _check_role(id, session) != 2:
            return Unauthorized("You need admin privileges.")

        data = query_select(user, user.email, email, session)

        if not data:
            return BadRequest("No such user.")
        query_delete(data, session)
        return NoContent()
    except:
        return BadRequest("Ooops, something went wrong...")


def delete_player(player_id: int, token: str, session) -> Unauthorized | BadRequest | NoContent:
    """Deletes a player profile

    Args:
        player_id (int): ID of the player to be deleted
        token (str): Bearer token
        session (_type_): ORM Session

    Returns:
        Unauthorized if you're not an admin | BadRequest if player is not found or is playing in a tourney| NoContent if successful
    """
    try:
        id = _is_authenticated(token)

        if _check_role(id, session) != 2:
            return Unauthorized("You need admin privileges.")

        data = query_select(player_profile , player_profile.profile_id, player_id, session)

        if not data:
            return BadRequest("No such player.")
        if data.current_tournament is not None:
            return BadRequest("Player is current playing in a tournament!")

        query_delete(data, session)
        return NoContent()
    except:
        BadRequest("Ooops, something went wrong...")


def delete_match(event_id: int, token: str, session) -> Unauthorized | BadRequest | NoContent:
    """Deletes a match

    Args:
        event_id (int): Match ID
        token (str): Bearer token
        session (_type_): ORM Session

    Returns:
        Unauthorized if you're not an admin | BadRequest of Player isn't found or is cur. playing in a tournament | NoContent If successful
    """
    try:
        id = _is_authenticated(token)

        if _check_role(id, session) != 2:
            return Unauthorized("You need admin privileges.")

        data = query_select(match , match.match_id, event_id, session)

        if not data:
            return BadRequest("No such player.")
        if data.tournament_id is not None:
            return BadRequest("The match is part of tournament!")

        query_delete(data, session)
        return NoContent()
    except:
        return BadRequest("Ooops, something went wrong...")


def delete_tournament(name: str, token: str, session) -> Unauthorized | BadRequest | NoContent:
    """Deletes a tournament
    Makes the current_tournament of all players involved "None"

    Args:
        name (str): Tournament name
        token (str): Bearer token
        session (_type_): ORM Session

    Returns:
        Unauthorized if you're not an admin | BadRequest if tournament isn't found | NoContent if successful
    """
    try:
        id = _is_authenticated(token)

        if _check_role(id, session) != 2:
            return Unauthorized("You need admin privileges.")

        data = query_select(tournament, tournament.title, name, session)

        if not data:
            return BadRequest("No such tournament.")

        players_in_tournament = queries_select(player_profile, player_profile.current_tournament, data.tournament_id, session)
        for player in players_in_tournament:
            player.current_tournament = None

        matches_in_tournament = queries_select(match, match.tournament_id, data.tournament_id, session)
        for each_match in matches_in_tournament:
            each_match.tournament_id = None

        general_tournament = queries_select(profile_tournament_records, profile_tournament_records.tournament_id, data.tournament_id, session)
        for each_row in general_tournament:
            query_delete(each_row, session)

        query_delete(data, session)

        return NoContent()
    except:
        BadRequest("Ooops, something went wrong...")
