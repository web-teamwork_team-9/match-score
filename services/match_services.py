from common.responses import Unauthorized, BadRequest, NoContent, Accepted
from common.database import *
from common.mail_notif import send_mail_add_match
from data.data_models import *
from services.users_services import _is_authenticated, _check_role
from services.players_services import _update_profile_ratio_statistics
from datetime import date, timedelta
from itertools import combinations
from sqlalchemy import inspect
import random


def create_match(event: match, token: str, session) -> Unauthorized | BadRequest | NoContent:
    """Creates new match
    1. Checks for a valid date
    2. Checks for valid player IDS
    3. Checks for valid match format
    4. If the profiles are connected to a user it sends them an email
    5. Inserts the new match into the database

    Args:
        event (match): Contains the match information
        token (str): Bearer token
        session (_type_): ORM Session

    Returns:
        Unauthorized: if bearer isn't admin | BadRequest | NoContent: if successful
    """
    id = _is_authenticated(token)

    if _check_role(id, session) == 0:
        return Unauthorized("You need to be an admin or director.")

    if event.date < date.today():
        return BadRequest("You cannot create a match to a previous date.")

    if event.player_one_id == event.player_two_id:
        return BadRequest(f"Player ids' should not be the same.")

    player_one = query_select(player_profile, player_profile.profile_id, event.player_one_id, session)
    player_two = query_select(player_profile, player_profile.profile_id, event.player_two_id, session)

    if not player_one:
        return BadRequest(f"Player with id: {event.player_one_id} does not exist.")
    if not player_two:
        return BadRequest(f"Player with id: {event.player_two_id} does not exist.")

    if event.format not in ["TL" , "SL"]:
        return BadRequest('Invalid match format. Must be "SL" or "TL"')

    try:
        user_one = query_select(user, user.user_id, player_one.user_id, session)
        user_two = query_select(user, user.user_id, player_two.user_id, session)

        if user_one.email is not None and user_two.email is not None:
            send_mail_add_match(user_one.email)
            send_mail_add_match(user_two.email)
    except:
        "Problem handled."

    return NoContent() if query_upsert(event, session) else BadRequest("Ooops, something went wrong...")


def create_tournament_matches(names: list, tour: tournament, session) -> list:
    """Creates matches for a tournament

    1. Shuffles the names randomly
    2. If the tournament is knockout we pair the names
    3. If the tournament is a league we find all possible unique pairing combinations
    4. We record each Profile and the current tournament in the profile_tournament_records
    5. We create a match a object for each pair

    Args:
        names (list): List of participant names
        tour (tournament): Tournament object
        session (_type_): ORM Session

    Returns:
        list: List of matches (not yet inserted into the database)
    """
    random.shuffle(names)
    if tour.format.lower() == "knockout":
        pairs = list(zip(names[::2], names[1::2]))
    else: pairs = list(combinations(names, 2))
    participant_profiles = {one:each for each in query_select_multiple(player_profile, player_profile.name, names, session) for one in names if one == each.name}
    matches = []

    league_records = [profile_tournament_records(player_id = each.profile_id, tournament_id = each.current_tournament) for each in participant_profiles.values()]
    new_records = []
    for record in league_records:
        check_db = query_select_two_params(profile_tournament_records, profile_tournament_records.player_id,
        record.player_id, profile_tournament_records.tournament_id, record.tournament_id, session)

        if not check_db:
            new_records.append(record)

    query_upsert(new_records, session)

    for each in pairs:
        player_one = participant_profiles[each[0]]
        player_two = participant_profiles[each[1]]
        new_match = match(date = date.today() + timedelta(days = 1), format = tour.match_format, player_one_id = player_one.profile_id, player_two_id = player_two.profile_id, tournament_id = tour.tournament_id)
        matches.append(new_match)

    return matches


def get_all(session) -> list | BadRequest:
    """Finds all matches in the DB

    Args:
        session (_type_): _ORM Session

    Returns:
        list of all matches found | BadRequest: if there are no matches in the DB
    """
    data = query_select_no_column(match, session)
    well_ordered = []
    for the_match in data:
        new = {key: getattr(the_match, key)
            for key in inspect(the_match).mapper.column_attrs.keys()}
        well_ordered.append(new)
    return well_ordered if data else BadRequest("No matches found.")


def get_event_id(id: int, session) -> dict | BadRequest:
    """Finds a match by ID in the DB
    If we find a match and return in straight away the attributes would be shuffled
    We perfom a dict compr. to orderd the attributes

    Args:
        id (int): Match ID
        session (_type_): ORM Session

    Returns:
        dict | BadRequest: If no match is found
    """
    data = query_select(match, match.match_id, id, session)
    return {key: getattr(data, key)
            for key in inspect(data).mapper.column_attrs.keys()} if data else BadRequest("No such match.")


def update_normal_match(event_id: int, event: match_score, token: str, session) -> Unauthorized | BadRequest | NoContent:
    """Updates a match
    1. Checks Bearer token role
    2. Finds the match we want to update in the DB or returns BadRequest if there is no such match.
    3. Checks if the match already has a winner, which would mean that it's already been updated
    4. Checks for the correct duration if match_format is "SL" (Score Limit)
    5. Updates the match object from the DB with the new data
    6. If it's not a tournament match:
    6.1 Updates the match in the DB
    6.2 Goes to update the profile statistics

    Args:
        event_id (int): Match ID
        event (match_score): New match data
        token (str): Bearer token
        session (_type_): ORM Session

    Returns:
        Unauthorized | BadRequest | NoContent
    """
    id = _is_authenticated(token)

    if _check_role(id, session) == 0:
        return Unauthorized("You need to be an admin or director.")

    data = query_select(match, match.match_id, event_id, session)
    if not data:
        return BadRequest("No such match.")

    if data.winner != None:
        return BadRequest("This match is already finished.")

    if data.format == "SL":
        if not _check_duration(event.duration):
            return BadRequest("Invalid duration.")

    # if data.date > date.today():
    #     return BadRequest(f"You cannot update a match before the day it's being played. ({data.date})")

    data.score_player_one, data.score_player_two, data.duration = event.score_player_one, event.score_player_two, event.duration
    if data.format == "TL":
        data.duration = "60:00"

    if event.score_player_one > event.score_player_two:
        data.winner = 1
    elif event.score_player_one < event.score_player_two:
        data.winner = 2
    else: data.winner = 0

    if data.tournament_id is not None:
        tour = query_select(tournament, tournament.tournament_id, data.tournament_id, session)
        if tour.format == "knockout":
            return _update_knockout_match(tour, data, session)
        else:
            return _update_league_match(tour, data, session) or BadRequest("Ooops, something went wrong...")

    new_data = query_upsert(data, session)

    _update_profile_ratio_statistics(new_data, session)

    return new_data


def _update_knockout_match(tour: tournament, data: match, session) -> None | BadRequest | Accepted:
    """ Updates a match from a knockout tournament
    1. Checks the score for draw
    2. Eliminates the loser from the tournament by selecting him from the DB and sets his cur. tourn. to None
    3. Each knockout tourn has stages with a number of matches to be played. If it hits 0:
    3.1 Selects all the players names with cur tourn (this tournament) from the DB
    3.2 Creates the new matches for the next stage and ends
    3.3 If there's only one player left from 3.1
    3.4 The record with this tourn and this player is updated to show that he won the tournament
    3.5 The cur tourn of the player becomes None
    3.6 Updates the DB with the new data
    3.7 Calls the profile updating function and continues uploading the new data to the DB

    Args:
        tour (tournament): Tournament object
        data (match): Match object
        session (_type_): ORM Session

    Returns:
        None | BadRequest | Accepted
    """
    tour.matches_to_play -= 1

    if data.winner == 1:
        loser_id = data.player_two_id
    elif data.winner == 2:
        loser_id = data.player_one_id
    else:
        return BadRequest("A knockout match cannot be a draw!")

    loser = query_select(player_profile, player_profile.profile_id, loser_id, session)
    loser.current_tournament = None
    query_upsert(loser, session)

    if tour.matches_to_play == 0:
        participants = [player.name for player in queries_select(player_profile, player_profile.current_tournament, tour.tournament_id, session)]
        tour.matches_to_play = int(len(participants) / 2)
        if len(participants) > 1:
            query_upsert(create_tournament_matches(participants, tour, session), session)
        else:
            tour.finished = 1
            if not participants:
                return BadRequest("Ooops, something went wrong...")
            winner = query_select(player_profile, player_profile.name, participants[0], session)
            record = query_select_two_params(profile_tournament_records, profile_tournament_records.player_id, winner.profile_id, profile_tournament_records.tournament_id, tour.tournament_id, session)
            record.is_winner = 1
            winner.current_tournament = None
            query_upsert(winner, session)
            query_upsert(record, session)

    query_upsert(data, session)
    _update_profile_ratio_statistics(data, session)
    query_upsert(tour, session)
    return Accepted("Match updated successfully.")


def _update_league_match(tour: tournament, data: match, session) -> BadRequest | None | Accepted:
    """Updates match from a league tournament
    1. Checks who the winner
    1.1 Selects his record and adds score
    1.2 Updates his score in the DB
    2. IF This is the last match
    2.1 Selects all participants from the DB
    2.2 Sets their cur tourn to 0 and updates the DB
    2.3 Selects all the records for this tourn from the DB
    2.4 Find the profile with the highest score and marks it as a winner
    3. Updates the DB
    4. Calls the profile statistics update

    Args:
        tour (tournament): League tournament object
        data (match): Match object
        session (_type_): ORM Session

    Returns:
        BadRequest | None | Accepted
    """
    tour.matches_to_play -= 1

    if data.winner == 1:
        player_league_obj = query_select_two_params(profile_tournament_records, profile_tournament_records.player_id, data.player_one_id, profile_tournament_records.tournament_id, tour.tournament_id, session)
        player_league_obj.score += 2
        query_upsert(player_league_obj, session)

    elif data.winner == 2:
        player_league_obj = query_select_two_params(profile_tournament_records, profile_tournament_records.player_id, data.player_two_id, profile_tournament_records.tournament_id, tour.tournament_id, session)
        player_league_obj.score += 2
        query_upsert(player_league_obj, session)

    elif data.winner == 0:
        player1_league_obj = query_select_two_params(profile_tournament_records, profile_tournament_records.player_id, data.player_one_id, profile_tournament_records.tournament_id, tour.tournament_id, session)
        player2_league_obj = query_select_two_params(profile_tournament_records, profile_tournament_records.player_id, data.player_two_id, profile_tournament_records.tournament_id, tour.tournament_id, session)
        player1_league_obj.score += 1
        player2_league_obj.score += 1
        query_upsert([player1_league_obj, player2_league_obj], session)
    else:
        return BadRequest("Ooops, something went wrong...")

    if tour.matches_to_play == 0:
        tour.finished = 1
        left_players = queries_select(player_profile, player_profile.current_tournament, tour.tournament_id, session)
        for each in left_players:
            each.current_tournament = None
        query_upsert(left_players, session)

        score_records = queries_select(profile_tournament_records, profile_tournament_records.tournament_id, tour.tournament_id, session)
        current_max_score = 0
        current_winner_record = None
        for record in score_records:
            if record.score > current_max_score:
                current_max_score = record.score
                current_winner_record = record
        current_winner_record.is_winner = 1
        query_upsert(current_winner_record, session)

    query_upsert(data, session)
    query_upsert(tour, session)
    _update_profile_ratio_statistics(data, session)
    return Accepted("Match updated successfully.")


def _check_duration(duration: str) -> bool:
    """ Function to use instead of Regex
    1. Checks if we're not sending an emtpy string
    2. Checks if string contains ':'
    3. Checks the lenghts of each split arround the ':'

    Args:
        duration (str): THe duration that we want to check. Should look like '40:52'

    Returns:
        bool
    """
    if duration == None:
        return False

    if ":" not in duration:
        return False

    listed = duration.split(":")
    first, second = listed

    if not (len(first) in [2,3] and first.isnumeric()):
        return False
    if not (len(second) == 2 and second.isnumeric()):
        return False

    return True