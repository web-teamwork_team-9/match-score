from common.responses import Conflict, BadRequest, Unauthorized, NoContent
from common.database import *
from common.secret import SECRET_KEY
from common.mail_notif import send_mail_new_user, send_mail_dir_req_approved, send_mail_dir_req_declined, send_mail_link_req_approved
from data.data_models import player_profile, user
from jwt import encode, decode, get_unverified_header
from hashlib import sha256
import uuid, re

EMAIL = re.compile("([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+")


def _hash_password(password: str) -> str:

    """
    Hashes the password
    """
    return sha256(password.encode('utf-8')).hexdigest()


def _find_by_email(email: str, session) -> list:
    """
    Returns the given emails's usern object
    """
    data = query_select(user, user.email, email, session)
    return data


def try_login(email: str, password: str, session) -> user | None:
    """
    Checks if the input data == database data

    Args:
        username (str): Username
        password (str): Password

    Returns:
        User if the credentials match or None if they don't
    """
    user_data = _find_by_email(email, session)
    if not user_data: return False
    password = _hash_password(password)
    return user_data if user_data.email == email and user_data.password == password else False


def create_token(email: str, session) -> str:
    """
    Creates a token for a user
    """
    data = query_select(user, user.email, email, session)
    try:
        _PAYLOAD_DATA = {"id": data.user_id, "email": data.email}
    except:
        return BadRequest("Bad data!")

    return encode(payload=_PAYLOAD_DATA, key=SECRET_KEY)


def register(email: str, password: str, session) -> Conflict | NoContent | BadRequest:
    """ Registers a new user
    1. Checks if email is correct format
    2. Checks if the password is the correct lenght
    3. Checks if the email is already registered
    4. Hashes the password
    5. Creates a new user
    6. Sends the user a welcoming email
    7. Uploads the new user object to the DB

    Args:
        username (str): Username
        password (str): Password
        email (str): Email
        role (int): Role, usually 0

    Returns:
        User object
    """
    if EMAIL.match(email) is None:
        return BadRequest("Invalid email.")

    if len(password) < 8:
        return BadRequest("Invalid password. Must be at least 8 characters.")

    if _find_by_email(email, session):
        return Conflict("Email already taken.")
    password = _hash_password(password)
    new_user = user(user_id = str(uuid.uuid4()), email = email, password = password)
    send_mail_new_user(new_user.email)
    
    return NoContent() if query_upsert(new_user, session) else BadRequest("Ooops, something went wrong...")


def post_promotion(token: str, session) -> ValueError | NoContent | BadRequest:
    """A user has requested to be promoted to director
    1. Checks if the user is registered
    2. Selects the user from the DB
    3. Updates requested to 1
    4. Updates the DB

    Args:
        token (str): Bearer Token
        session (_type_): ORM Session

    Returns:
        ValueError | NoContent | BadRequest
    """
    id = _is_authenticated(token)
    try:
        if id is False:
            raise ValueError()
        data = query_select(user, user.user_id, id, session)
        data.requested = 1
        query_upsert(data, session)
        return NoContent()
    except:
        return BadRequest("Ooops, something went wrong...")


def get_promotions(token: str, session) -> dict:
    """Finds all promotion requests
    1. Checks the role of the Bearer Token
    2. Selects all user that have requested on 1

    Args:
        token (str): Bearer Token
        session (_type_): ORM Session

    Returns:
        dict of all requested users
    """
    id = _is_authenticated(token)

    if _check_role(id, session) != 2:
        return Unauthorized("You need admin privileges.")
    data = queries_select(user, user.requested, 1, session)

    users = {}
    for number, each in enumerate(data):
        users[number] = each.email
    return users


def req_for_director(email: str, action: str, token: str, session) -> BadRequest | Unauthorized | NoContent:
    """Used when a user has put on a request to become a director
    1. Checks if Bearer token is valid
    2. Checks the Bearer token role
    3. Selects the user object from the DB
    4. Checks the action
    4. Changes the object data
    5. Uploads the updated object in the DB
    6. Sends an email

    Args:
        email (str): email of the user to become a director
        action (str): either 'approve' or 'decline'
        token (str): Bearer Token
        session (_type_): ORM Session

    Returns:
        BadRequest | Unauthorized | NoContent
    """
    id = _is_authenticated(token)
    if not id:
        return BadRequest("Ooops, something went wrong...")

    if _check_role(id, session) != 2:
        return Unauthorized("You need admin privileges.")

    data = query_select(user, user.email, email, session)

    if action.lower() == "approve":
        data.role, data.requested = 1, 0
        query_upsert(data, session)
        send_mail_dir_req_approved(data.email)

    elif action.lower() == "decline":
        data.role, data.requested = 0, 0
        query_upsert(data, session)

        send_mail_dir_req_declined(data.email)

    else:
        return BadRequest("Invalid keyword for action provided! Approve or decline only valid!")

    return NoContent()


def post_profile(profile_id: int, token: str, session) -> ValueError | NoContent | BadRequest:
    """Requests a connection from a user to a player profile
    1. Checks the Token
    2. Checks if the profile exists
    3. Checks if the profile is already linked
    4. Checks if the user is already linked to another user
    5. Selects the user object from the DB
    6. Updates the connection request
    7. Uploades the new data to the DB

    Args:
        profile_id (int): Profile to request a link to
        token (str): Bearer Token
        session (_type_): ORM Session

    Returns:
        ValueError | NoContent | BadRequest
    """
    id = _is_authenticated(token)
    try:
        if not id:
            return BadRequest("You need to be logged in.")
        profile = query_select(player_profile, player_profile.profile_id, profile_id, session)
        if not profile:
            return BadRequest(f"Player profile with id:{profile_id} doesn't exist.")

        if profile.user_id:
            return BadRequest("This profile is already linked to another user.")

        if query_select(player_profile, player_profile.user_id, id, session):
            return BadRequest("You are already connected with another player profile.")

        data = query_select(user, user.user_id, id, session)
        data.connect = profile_id
        query_upsert(data, session)

        return NoContent()
    except:
        return BadRequest("Ooops, something went wrong...")


def get_profiles(token: str, session) -> Unauthorized | dict:
    """ Finds all users from DB
    1. Checks Token role
    2. Selects all users from DB
    3. Enumerates the list
    4. Returns a dict with each users email

    Args:
        token (str): Bearer token
        session (_type_): ORM Session

    Returns:
        Unauthorized | dict:
    """
    id = _is_authenticated(token)

    if _check_role(id, session) == 0:
        return Unauthorized("You need to be admin or director.")
    data = queries_select_not_value(user, user.connect, 0, session)

    users = {}
    for number, each in enumerate(data):
        users[f"Email - {each.email}"] = each.connect
    return users


def connect_profile(email: str, token: str, session) -> BadRequest | Unauthorized | NoContent:
    """ Connects a user to a player profile
    1. Checks the Token
    2. Checks the Token role
    3. Selects the user with this email from the DB
    4. Checks if the profile.user_id is free
    5. Updates the profile and user objects
    6. Uploads updated data to the DB

    Args:
        email (str): User email who wants to connect
        token (str): Bearer Token
        session (_type_): ORM Session

    Returns:
        BadRequest | Unauthorized | NoContent
    """
    id = _is_authenticated(token)

    if id is False:
        return BadRequest("Ooops, something went wrong...")

    if _check_role(id, session) == 0:
        return Unauthorized("You need admin privileges.")

    data = query_select(user, user.email, email, session)
    if not data:
        return BadRequest("Invalid user email.")

    player = query_select(player_profile, player_profile.profile_id, data.connect, session)

    if player.user_id in [None, "0", data.user_id]:
        pass
    else:
        return BadRequest("Player profile already connected to another user")

    data.connect, player.user_id = 0, data.user_id
    query_upsert(data, session)
    query_upsert(player, session)
    send_mail_link_req_approved(data.email)
    
    return NoContent()


def _is_authenticated(token: str) -> int | bool:
    """
    Checks if a user is logged in
    """
    try:
        header_data = get_unverified_header(token)
        decoded_token = decode(token, key=SECRET_KEY, algorithms=[header_data['alg'], ])
        return decoded_token['id']
    except:
        return False


def _check_role(id: str, session) -> int | BadRequest:
    """
    Checks if a user is an Admin
    """
    try:
        data = query_select(user, user.user_id, id, session)
        return data.role
    except:
        return BadRequest("Ooops, something went wrong...")
