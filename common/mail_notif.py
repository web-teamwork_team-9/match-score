from mailjet_rest import Client
from common.secret import MAIL_API_KEY, MAIL_SECRET_KEY


def send_mail_new_user(username_mail: str) -> bool:

    mailjet = Client(auth=(MAIL_API_KEY, MAIL_SECRET_KEY), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {"Email": "info@match-score.xyz", "Name": "Match Score"},
                "To": [{"Email": username_mail, "Name": "passenger 1"}],
                "TemplateID": 4322551,
                "TemplateLang": True,
                "Subject": "Welcome to match-score.xyz!",
            }
        ]
    }

    try:
        mailjet.send.create(data=data)
        return True
    except:
        return False


def send_mail_add_tourn(username_mail: str) -> bool:

    mailjet = Client(auth=(MAIL_API_KEY, MAIL_SECRET_KEY), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {"Email": "info@match-score.xyz", "Name": "Match Score"},
                "To": [{"Email": username_mail, "Name": "passenger 1"}],
                "TemplateID": 4345046,
                "TemplateLang": True,
                "Subject": "You were added to tournament!",
            }
        ]
    }

    try:
        mailjet.send.create(data=data)
        return True
    except:
        ValueError("Error!")
        return False


def send_mail_add_match(username_mail: str) -> bool:

    mailjet = Client(auth=(MAIL_API_KEY, MAIL_SECRET_KEY), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {"Email": "info@match-score.xyz", "Name": "Match Score"},
                "To": [{"Email": username_mail, "Name": "passenger 1"}],
                "TemplateID": 4345037,
                "TemplateLang": True,
                "Subject": "You were added to match!",
            }
        ]

    }

    try:
        mailjet.send.create(data=data)
        return True
    except:
        return False


def send_mail_dir_req_approved(username_mail: str) -> bool:

    mailjet = Client(auth=(MAIL_API_KEY, MAIL_SECRET_KEY), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {"Email": "info@match-score.xyz", "Name": "Match Score"},
                "To": [{"Email": username_mail, "Name": "passenger 1"}],
                "TemplateID": 4345094,
                "TemplateLang": True,
                "Subject": "You've been promoted to director!",
            }
        ]
    }

    try:
        mailjet.send.create(data=data)
        return True
    except:
        return False


def send_mail_dir_req_declined(username_mail: str) -> bool:

    mailjet = Client(auth=(MAIL_API_KEY, MAIL_SECRET_KEY), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {"Email": "info@match-score.xyz", "Name": "Match Score"},
                "To": [{"Email": username_mail, "Name": "passenger 1"}],
                "TemplateID": 4345102,
                "TemplateLang": True,
                "Subject": "Director request declined!",
            }
        ]
    }

    try:
        mailjet.send.create(data=data)
        return True
    except:
        return False


def send_mail_link_req_approved(username_mail: str) -> bool:

    mailjet = Client(auth=(MAIL_API_KEY, MAIL_SECRET_KEY), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {"Email": "info@match-score.xyz", "Name": "Match Score"},
                "To": [{"Email": username_mail, "Name": "passenger 1"}],
                "TemplateID": 4345596,
                "TemplateLang": True,
                "Subject": "Successful link to player profile!",
            }
        ]
    }

    try:
        mailjet.send.create(data=data)
        return True
    except:
        return False


def send_mail_link_req_declined(username_mail: str) -> bool:

    mailjet = Client(auth=(MAIL_API_KEY, MAIL_SECRET_KEY), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {"Email": "info@match-score.xyz", "Name": "Match Score"},
                "To": [{"Email": username_mail, "Name": "passenger 1"}],
                "TemplateID": 4345601,
                "TemplateLang": True,
                "Subject": "Player profile link declined!",
            }
        ]
    }

    try:
        mailjet.send.create(data=data)
        return True
    except:
        return False


def send_mail_match_updated(username_mail: str) -> bool:

    mailjet = Client(auth=(MAIL_API_KEY, MAIL_SECRET_KEY), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {"Email": "info@match-score.xyz", "Name": "Match Score"},
                "To": [{"Email": username_mail, "Name": "passenger 1"}],
                "TemplateID": 4345587,
                "TemplateLang": True,
                "Subject": "Match information updated!",
            }
        ]
    }

    try:
        mailjet.send.create(data=data)
        return True
    except:
        return False