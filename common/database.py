from sqlmodel import create_engine, Session, select
import copy
import os
from common.secret import DB_URL



engine = create_engine(DB_URL)


def query_upsert(table, session):
    if type(table) is list: session.bulk_save_objects(table)
    else: session.add(table)
    table_copy = copy.deepcopy(table)
    session.commit()
    return table_copy


def query_select(table, column, value, session):
    statement = select(table).where(column == value)
    query = session.exec(statement).first()
    return query


def queries_select(table, column, value, session):
    statement = select(table).where(column == value)
    query = session.exec(statement).all()
    return query


def queries_select_not_value(table, column, value, session):
    statement = select(table).where(column != value)
    query = session.exec(statement).all()
    return query


def query_select_multiple(table, column, value, session):
    statement = select(table).where(column.in_(value))
    query = session.exec(statement).all()
    return query


def query_select_no_column(table, session):
    statement = select(table)
    query = session.exec(statement).all()
    return query


def query_delete(table, session):
    session.delete(table)
    session.commit()


def query_select_two_params(table, column1, value1, column2, value2, session):
    statement = select(table).where(column1 == value1).where(column2 == value2)
    query = session.exec(statement).first()
    return query


def queries_select_two_params(table, column1, value1, column2, value2, session):
    statement = select(table).where(column1 == value1).where(column2 == value2)
    query = session.exec(statement).all()
    return query


def query_select_player_record_where_winner(table, column, value, session):
    statement = select(table).where(column == value)
    query = session.exec(statement).all()
    data = []
    for record in query:
        if record.is_winner == 1:
            data.append(record)
    return query


def get_session():
    with Session(engine) as session:
        yield session
