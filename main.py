from routers.tournament_routers import tournaments_router
from routers.players_routers import players_router
from routers.users_routers import users_router
from routers.match_routers import match_router
from routers.admin_routers import admin_router
from common.database import engine
from sqlmodel import SQLModel
from fastapi import FastAPI

# SQLModel.metadata.create_all(engine)

app = FastAPI()
app.include_router(tournaments_router)
app.include_router(players_router)
app.include_router(users_router)
app.include_router(match_router)
app.include_router(admin_router)
