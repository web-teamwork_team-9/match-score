# Alpha40 Team 9 Final Project - Match score 

![Cover image](static/cover.png)

[![Pipeline status](https://gitlab.com/web-teamwork_team-9/match-score/badges/main/pipeline.svg)](https://gitlab.com/web-teamwork_team-9/match-score/-/commits/main) [![Coverage](https://gitlab.com/web-teamwork_team-9/match-score/badges/main/coverage.svg)](https://gitlab.com/web-teamwork_team-9/match-score/-/commits/main)

Link: [https://match-score-heroku.herokuapp.com/](https://match-score-heroku.herokuapp.com/)

## About the project
Final project in the Python Alpha 40 course of Telerik Academy.

Back-end RESTful API system for management of sports tournaments with mail notifications, different types of tournaments, different user roles with permissions to control and access the system, and generated detailed statistics.

Develop by: [Nikolay Angelov](https://gitlab.com/nangelovv), [Ivo Georgiev](https://gitlab.com/IvoGeorgiev) and [Ventsislav Kostadinov](https://gitlab.com/ven.kostadinov)

## Technologies:
1. Python (duhh)
2. FastAPI
3. Uvicorn
4. SQLModel (ORM) - combines Pydantic and SQLAlchemy
5. MySQL and SQLite3
6. unittest
7. Continuous Integration
8. Continuous Deployment
9. JWT, mailjet_rest and other small but useful libs

## Database schema:
![DB Schema](static/db_schema.png)

## How to set up and run
1. Clone the repo `git clone https://gitlab.com/web-teamwork_team-9/match-score`
2. Create a new database schema
3. Add the new database link to DB_URL in the common/secret file
4. Change the other values in secret.py to random values. More Info below
5. Run the project once. This will automatically create the DB tables.
6. Run the MatchScoreViewScript.sql file connected to the DB(local or remote) you use using CLI or GUI software like Workbench or DB Browser for SQLite. This will create the final view table.
7. Enjoy

### Edit the environment variables file
The file **should** contain 4 variables with exactly the same names as below:

```bash
DB_URL=

SECRET_KEY=

MAIL_API_KEY=

MAIL_SECRET_KEY=
```

*the ***DB_URL*** is the exact URL to connect to your remote DB (see SQLMOdel for diff types) if you're keeping it simple with SQLite you could insert sqlite:///NAME.db*

*the **SECRET_KEY** is the secret key for generating JWT tokens. Generate some long string with password generator*

*the **MAIL_API_KEY** and **MAIL_SECRET_KEY** are the API keys for [mailjet.com library](http://mailjet.com) that we use for notifications. Register on the mailjet site and generate your keys. This is **NOT** essential and the program will run without them without any problems*

## Thanks
We would like to thank Stayko Tzenov. Our mentor for the project. For the guidance, good examples, shared experience and support.

We would like also to thank our trainers in the Telerik Academy - Edo, Vladi and Bobi. Thank you all for all you taught us and shared with us. 😀
