from sqlmodel import Field, SQLModel
from typing import Optional
from datetime import date


class user(SQLModel, table = True):
    user_id: str = Field(primary_key = True)
    email: str = Field(index = True)
    password: str
    role: int = 0
    requested: int = 0
    connect: int = 0


class tournament(SQLModel, table = True):
    tournament_id: str = Field(primary_key = True)
    title: str = Field(index = True)
    format: str
    match_format: str
    prize: str
    matches_to_play: Optional[int]
    finished: Optional[int]


class match(SQLModel, table = True):
    match_id: Optional[int] = Field(primary_key = True, index = True)
    date: date
    score_player_one: Optional[int]
    score_player_two: Optional[int]
    winner: Optional[int]
    duration: Optional[str]
    tournament_id: Optional[str] = Field(foreign_key = "tournament.tournament_id")
    player_one_id: Optional[int] = Field(foreign_key = "player_profile.profile_id")
    player_two_id: Optional[int] = Field(foreign_key = "player_profile.profile_id")
    format: str


class player_profile(SQLModel, table = True):
    profile_id: Optional[int] = Field(primary_key = True, index = True)
    name: str
    country: str
    tournaments_played: Optional[int]
    tournaments_won: Optional[int]
    matches_played: Optional[int] = 0
    matches_won: Optional[int] = 0
    most_often_opponent: Optional[int]
    best_opponent: Optional[int]
    worst_opponent: Optional[int]
    best_opp_w_l_ratio: Optional[float]
    worst_opp_w_l_ratio: Optional[float]
    club_name: Optional[str]
    user_id: Optional[str] = Field(foreign_key = "user.user_id")
    current_tournament: Optional[str] = Field(foreign_key = "tournament.tournament_id")


class profile_tournament_records(SQLModel, table = True):
    random_id: Optional[int] = Field(primary_key = True)
    tournament_id: str = Field(foreign_key = "tournament.tournament_id")
    player_id: int = Field(foreign_key = "player_profile.profile_id")
    score: int = 0
    is_winner: int = 0


class LoginData(SQLModel, table = False):
    email: str
    password: str


class match_score(SQLModel, table = False):
    score_player_one: int
    score_player_two: int
    duration: Optional[str]


class tournament_creation(SQLModel, table = False):
    title: str
    format: str
    participants: list
    match_format: str
    prize: str


class profile_updates(SQLModel, table = False):
    name: Optional[str]
    country: Optional[str]
    club_name: Optional[str]
    user_id: Optional[str]


class rivalry(SQLModel, table = True):
    player_one_id: Optional[int] = Field(primary_key = True)
    player_two_id: Optional[int] = Field(primary_key = True)
    win_ratio: float
    total_games: int