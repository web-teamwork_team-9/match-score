from sqlmodel import Session, SQLModel, create_engine
from services.users_services import *
from data.data_models import *
from common.responses import *
from services.users_services import _find_by_email, _hash_password, _check_role, _is_authenticated
from unittest.mock import patch
from os import path, remove
import unittest


class MockSession:
    @classmethod
    def setUp(self):
        self.flag = True
        self.DB_ENGINE = create_engine("sqlite:///testing.db", connect_args={"check_same_thread": False})
        self.SESSION = Session(self.DB_ENGINE)
        SQLModel.metadata.create_all(self.DB_ENGINE)
        return self.SESSION


    @classmethod
    def tearDown(self):
        self.flag = True
        self.SESSION.close()
        if path.exists("testing.db"):
            remove("testing.db")


class FindEmail_Should(unittest.TestCase):
    @patch('services.users_services.query_select', return_value = True)
    def test_find_by_email_SUCCESS(self, patch1):
        session = MockSession.setUp()
        result = _find_by_email("test@abv.bg", session)
        self.assertTrue(result)
        MockSession.tearDown()


    def test_find_by_email_FAIL(self):
        session = MockSession.setUp()
        result = _find_by_email("test@abv.bg", session)
        self.assertFalse(result)
        MockSession.tearDown()


class TryLogin_Should(unittest.TestCase):
    def test_try_login_SUCCESS(self):
        session = MockSession.setUp()
        register("test22@abv.bg", "12345678", session)
        result = try_login("test22@abv.bg", "12345678", session)
        self.assertIsInstance(result, user)
        MockSession.tearDown()


    @patch('services.users_services._find_by_email', return_value = None)
    def test_try_login_WRONG_EMAIL(self, patch1):
        session = MockSession.setUp()
        result = try_login("test@abv.bg", "12345", session)
        self.assertFalse(result)
        MockSession.tearDown()


    def test_try_login_EMAIL_MISMATCH(self):
        session = MockSession.setUp()
        register("tes2t@abv.bg", "12345", session)

        result = try_login("test@abv.bg", "12345", session)
        self.assertFalse(result)
        MockSession.tearDown()


class CreateToken_Should(unittest.TestCase):
    def test_create_token_SUCCESS(self):
        session = MockSession.setUp()
        test_mail = "test12@abv.bg"
        test_pass = "12345678"

        register(test_mail, test_pass, session)
        result = create_token(test_mail, session)
        self.assertIsInstance(result, str)
        self.assertIsNotNone(result)
        MockSession.tearDown()


    def test_create_token_FAIL_NO_ENTRY(self):
        session = MockSession.setUp()
        test_mail = "test12@abv.bg"

        result = create_token(test_mail + "2", session)
        self.assertIsInstance(result, BadRequest)
        self.assertIsNotNone(result)
        MockSession.tearDown()


class RegisterUser_Should(unittest.TestCase):
    @patch('services.users_services._find_by_email', return_value = True)
    def test_register_CONFLICT(self, patch1):
        session = MockSession.setUp()
        result = register("neshto@abv.bg", "12345678", session)
        self.assertIsInstance(result, Conflict)
        self.assertIsNotNone(result)
        MockSession.tearDown()


    @patch('services.users_services._find_by_email', return_value = False)
    def test_register_SUCCESS(self, patch1):
        session = MockSession.setUp()
        result = register("neshto@abv.bg", "12345678", session)
        self.assertIsInstance(result, NoContent)
        self.assertIsNotNone(result)
        MockSession.tearDown()


    @patch('services.users_services.query_upsert', return_value = None)
    @patch('services.users_services._find_by_email', return_value = False)
    def test_register_BADREQUEST(self, patch1, patch2):
        session = MockSession.setUp()
        result = register("neshto@abv.bg", "12345678", session)
        self.assertIsInstance(result, BadRequest)
        self.assertIsNotNone(result)
        self.assertEqual(result.body, b"Ooops, something went wrong...")
        MockSession.tearDown()


class PostPromotion_Should(unittest.TestCase):
    @patch('services.users_services._is_authenticated', return_value = False)
    def test_post_promotion_WRONG_ID(self, patch1):
        session = MockSession.setUp()
        result = post_promotion("fake_token", session)
        self.assertEqual(result.body, b"Ooops, something went wrong...")
        MockSession.tearDown()


    def test_post_promotion_SUCCESS(self):
        session = MockSession.setUp()
        test_mail = "test12@abv.bg"
        test_pass = "12345678"

        register(test_mail, test_pass, session)
        token = create_token(test_mail, session)
        result = post_promotion(token, session)
        self.assertIsInstance(result, NoContent)
        MockSession.tearDown()


class CheckRole_Should(unittest.TestCase):
    def test_check_role_SUCCESS(self):
        session = MockSession.setUp()
        test_mail = "test12@abv.bg"
        test_pass = "12345678"

        register(test_mail, test_pass, session)
        user_data = try_login(test_mail, test_pass, session)

        result = _check_role(user_data.user_id, session)
        self.assertIsInstance(result, int)
        self.assertIsNotNone(result)
        MockSession.tearDown()


    def test_check_role_FAILED(self):
        session = MockSession.setUp()

        result = _check_role("fake-id", session)
        self.assertIsInstance(result, BadRequest)
        self.assertEqual(result.body, b"Ooops, something went wrong...")
        MockSession.tearDown()


class IsAuthenticated_Should(unittest.TestCase):
    @patch('services.users_services.decode', return_value = {
        "id": 123,
        "email": "test@abv.bg"
    })
    @patch('services.users_services.get_unverified_header', return_value = {
        "alg": "HS256",
        "typ": "JWT"
    })
    def test_is_authenticated_SUCCESS(self, patch1, patch2):
        result = _is_authenticated("fake-token")
        self.assertIsInstance(result, int)
        self.assertTrue(result)


    @patch('services.users_services.decode', return_value = {
        "id": 123,
        "email": "test@abv.bg"
    })
    def test_is_authenticated_FAIL(self, patch1):
        result = _is_authenticated(123)
        self.assertFalse(result)
        self.assertIsInstance(result, bool)


class ConnectProfile_Should(unittest.TestCase):
    @patch('services.users_services._is_authenticated', return_value = False)
    def test_connect_profiles_WRONG_AUTH(self, patch1):
        session = MockSession.setUp()
        result = connect_profile("test@abv.bg", "123", session)
        self.assertEqual(type(result), BadRequest)
        MockSession.tearDown()


    @patch('services.users_services._check_role', return_value = 0)
    @patch('services.users_services._is_authenticated', return_value = 123)
    def test_connect_profiles_WRONG_ROLE(self, patch1, patch2):
        session = MockSession.setUp()
        result = get_profiles("123", session)
        self.assertEqual(type(result), Unauthorized)
        MockSession.tearDown()

    #TODO to fix this
    # def test_connect_profiles_WRONG_ROLE(self):
    #     session = MockSession.setUp()
    #     result = get_profiles("123", session)
    #     self.assertEqual(type(result), Unauthorized)
    #     MockSession.tearDown()


class GetProfiles_Should(unittest.TestCase):
    @patch('services.users_services._check_role', return_value = 1)
    @patch('services.users_services._is_authenticated', return_value = 123)
    def test_get_profiles_SUCCESS(self, patch1, patch2):
        session = MockSession.setUp()
        result = get_profiles("123", session)
        self.assertIsInstance(result, dict)
        self.assertIsNotNone(result)
        MockSession.tearDown()


    @patch('services.users_services._check_role', return_value = 0)
    @patch('services.users_services._is_authenticated', return_value = 123)
    def test_get_profiles_WRONG_ROLE(self, patch1, patch2):
        session = MockSession.setUp()
        result = get_profiles("123", session)
        self.assertIsInstance(result, Unauthorized)
        self.assertTrue(result)
        self.assertEqual(result.body, b"You need to be admin or director.")
        MockSession.tearDown()


class PostProfile_Should(unittest.TestCase):
    def test_post_profile_WRONG_ID(self):
        session = MockSession.setUp()
        result = post_profile(123 ,"123", session)
        self.assertEqual(result.body, b"You need to be logged in.")
        self.assertIsNotNone(result)
        MockSession.tearDown()

    @patch('services.users_services.query_select', return_value = ValueError())
    @patch('services.users_services._is_authenticated', return_value = True)
    def test_post_profile_WRONG_ID2(self, patch1, patch2):
        session = MockSession.setUp()
        result = post_profile(123 ,"123", session)
        self.assertIsInstance(result, BadRequest)
        self.assertIsNotNone(result)
        self.assertEqual(result.body, b"Ooops, something went wrong...")
        MockSession.tearDown()

    #TODO to fix it
    # def test_post_profile_SUCCESS(self):
    #     session = MockSession.setUp()
    #     test_mail = "test12@abv.bg"
    #     test_pass = "12345"

    #     register(test_mail, test_pass, session)
    #     user_data = try_login(test_pass, test_mail, session)
    #     result = post_profile( 123, 123, session)
    #     self.assertEqual(type(result), NoContent)
    #     MockSession.tearDown()


class UpgradeUser_Should(unittest.TestCase):
    def test_upgrade_user_FAIL_AUTH(self):
        session = MockSession.setUp()
        result = req_for_director("test@abv.bg", "decline", "fake-token", session)

        self.assertIsInstance(result, BadRequest)
        self.assertIsNotNone(result)
        self.assertEqual(result.body, b"Ooops, something went wrong...")

        MockSession.tearDown()


    @patch('services.users_services._check_role', return_value = 1)
    @patch('services.users_services._is_authenticated', return_value = True)
    def test_upgrade_user_FAIL_ROLE(self, patch1, patch2):
        session = MockSession.setUp()
        result = req_for_director("test@abv.bg", "approve", "fake-token", session)

        self.assertIsInstance(result, Unauthorized)
        self.assertIsNotNone(result)
        self.assertEqual(result.body, b"You need admin privileges.")

        MockSession.tearDown()


    @patch('services.users_services._check_role', return_value = 2)
    @patch('services.users_services._is_authenticated', return_value = True)
    def test_upgrade_user_SUCCESS(self, patch1, patch2):
        session = MockSession.setUp()
        test_mail = "test12@abv.bg"
        test_pass = "12345678"

        register(test_mail, test_pass, session)
        result = req_for_director(test_mail, "approve", "fake-token", session)

        self.assertIsInstance(result, NoContent)
        self.assertIsNotNone(result)

        MockSession.tearDown()


class HashPassword_Should(unittest.TestCase):
    def test_upgrade_user_SUCCESS(self):
        result = _hash_password("123")
        self.assertEqual(type(result), str)
