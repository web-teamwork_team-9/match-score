from sqlmodel import Session, SQLModel, create_engine
from services.players_services import create_new_profile
from services.tournament_services import *
from services.tournament_services import _name_validation
from data.data_models import *
from common.responses import *
from unittest.mock import patch
from os import path, remove
import unittest


class MockSession:
    @classmethod
    def setUp(self):
        self.flag = True
        self.DB_ENGINE = create_engine("sqlite:///testing.db", connect_args={"check_same_thread": False})
        self.SESSION = Session(self.DB_ENGINE)
        SQLModel.metadata.create_all(self.DB_ENGINE)
        return self.SESSION


    @classmethod
    def tearDown(self):
        self.flag = True
        self.SESSION.close()
        if path.exists("testing.db"):
            remove("testing.db")


class CreateTournament_Should(unittest.TestCase):
    @patch('services.tournament_services.query_select', return_value = True)
    def test_create_NameExists_BADREQUEST(self, patch1):
        session = MockSession.setUp()
        result = create_tournament(tournament_creation(
            title = "Nameyy",
            format = "league",
            participants = ["Leonardo", "Penka"],
            match_format = "TL",
            prize = "None"), "fake-token", session)
        self.assertEqual(result.body, b"There is already a tournament named 'Nameyy'")
        MockSession.tearDown()


    @patch('services.tournament_services.query_select', return_value = False)
    def test_create_UNATHORIZED(self, patch1):
        with patch('services.tournament_services._check_role', return_value = 0):
            session = MockSession.setUp()
            result = create_tournament(tournament_creation(
                title = "Nameyy",
                format = "league",
                participants = ["Leonardo", "Penka"],
                match_format = "TL",
                prize = "None"), "fake-token", session)
            self.assertEqual(result.body, b"You need to be an admin or director.")
            MockSession.tearDown()


    @patch('services.tournament_services.query_select', return_value = False)
    def test_create_InvalidFormat_BADREQUEST(self, patch1):
        with patch('services.tournament_services._check_role', return_value = 1):
            session = MockSession.setUp()
            result = create_tournament(tournament_creation(
                title = "Nameyy",
                format = "wrong format",
                participants = ["Leonardo", "Penka"],
                match_format = "TL",
                prize = "None"), "fake-token", session)
            self.assertEqual(result.body, b"Invalid tournament format.")
            MockSession.tearDown()


    @patch('services.tournament_services.query_select', return_value = False)
    def test_create_InvalidParticipants_BADREQUEST(self, patch1):
        with patch('services.tournament_services._check_role', return_value = 2):
            session = MockSession.setUp()
            result = create_tournament(tournament_creation(
                title = "Nameyy",
                format = "league",
                participants = ["Leonardo"],
                match_format = "TL",
                prize = "None"), "fake-token", session)
            self.assertEqual(result.body, b"Invalid number of players for league tournament.")
            MockSession.tearDown()


    @patch('services.tournament_services.query_select', return_value = False)
    def test_create_SUCCESS(self, patch1):
        with patch('services.tournament_services._check_role', return_value = 2):
            session = MockSession.setUp()
            result = create_tournament(tournament_creation(
                title = "Nameyy",
                format = "knockout",
                participants = ["Leonardo", "Kenya"],
                match_format = "TL",
                prize = "None"), "fake-token", session)
            self.assertEqual(type(result), NoContent)
            MockSession.tearDown()


    @patch('services.tournament_services.query_select', return_value = False)
    @patch('services.tournament_services.query_upsert', return_value = ValueError())
    def test_create_UnknownError_BADREQUEST(self, patch1, patch2):
        with patch('services.tournament_services._check_role', return_value = 2):
            session = MockSession.setUp()
            result = create_tournament(tournament_creation(
                title = "Nameyy",
                format = "knockout",
                participants = ["Leonardo", "Kenya"],
                match_format = "TL",
                prize = "None"), "fake-token", session)
            self.assertEqual(result.body, b"Ooops, something went wrong...")
            MockSession.tearDown()


class GetAll_Should(unittest.TestCase):
    def test_get_all_UNSUCCESSFUL(self):
        session = MockSession.setUp()
        result = get_all(session)
        self.assertEqual(result.body, b"No tournaments found.")
        MockSession.tearDown()


    @patch('services.tournament_services.query_select', return_value = False)
    @patch('services.tournament_services._check_role', return_value = 2)
    def test_get_all_SUCCESSFUL(self, patch1, patch2):
        session = MockSession.setUp()
        create_tournament(tournament_creation(
                title = "Nameyy",
                format = "knockout",
                participants = ["Leonardo", "Penka"],
                match_format = "TL",
                prize = "None"), "fake-token", session)

        self.assertEqual(type(get_all(session)), list)
        MockSession.tearDown()


class GetEventName_Should(unittest.TestCase):
    def test_get_name_UNSUCCESSFUL(self):
        session = MockSession.setUp()
        result = get_event_name(1, session)
        self.assertEqual(result.body, b"No such tournament.")
        MockSession.tearDown()


    @patch('services.tournament_services._check_role', return_value = 2)
    def test_get_name_SUCCESSFUL(self, patch1):
        session = MockSession.setUp()
        with patch('services.tournament_services.query_select', return_value = False):
            create_tournament(tournament_creation(
                    title = "Nameyy",
                    format = "knockout",
                    participants = ["Leonardo", "Penka"],
                    match_format = "TL",
                    prize = "None"), "fake-token", session)

        self.assertEqual(type(get_event_name("Nameyy", session)), dict)
        MockSession.tearDown()


class CheckParticipantProfiles_Should(unittest.TestCase):
    def test_check_profiles_InvalidName_UNSUCCESSFUL(self):
        session = MockSession.setUp()
        result = check_participants_profiles(["Pesho4", "Penka"], "tour_id", session)
        self.assertEqual(result.body, b"Invalid player name.")
        MockSession.tearDown()


    def test_check_profiles_InTournament_UNSUCCESSFUL(self):
        session = MockSession.setUp()
        with patch('services.players_services._check_role', return_value = 2):
            create_new_profile(player_profile(name = "Pesho", country = "Oceania", current_tournament = "not tour_id"), "fake-token", session)
        result = check_participants_profiles(["Pesho"], "tour_id", session)
        self.assertEqual(result.body, b"Pesho is already in another tournament.")
        MockSession.tearDown()


    def test_check_profiles_SUCCESSFUL(self):
        session = MockSession.setUp()
        result = check_participants_profiles(["Pesho", "Penka"], "tour_id", session)
        self.assertEqual(type(result), list)
        MockSession.tearDown()


class NameValidation_Should(unittest.TestCase):
    def test_validate_UNSUCCESSFUL(self):
        self.assertFalse(_name_validation("Penka4", "tour_id"))


    def test_validate_SUCCESS(self):
        self.assertEqual(type(_name_validation("Penka", "tour_id")), player_profile)
