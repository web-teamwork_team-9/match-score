from sqlmodel import Session, SQLModel, create_engine
from services.tournament_services import create_tournament
from services.players_services import create_new_profile
from services.match_services import create_match
from services.users_services import register
from services.admin_services import *
from data.data_models import *
from common.responses import *
from unittest.mock import patch
from os import path, remove
import unittest


class MockSession:
    @classmethod
    def setUp(self):
        self.flag = True
        self.DB_ENGINE = create_engine("sqlite:///testing.db", connect_args={"check_same_thread": False})
        self.SESSION = Session(self.DB_ENGINE)
        SQLModel.metadata.create_all(self.DB_ENGINE)
        return self.SESSION


    @classmethod
    def tearDown(self):
        self.flag = True
        self.SESSION.close()
        if path.exists("testing.db"):
            remove("testing.db")


class DeleteUser_Should(unittest.TestCase):
    @patch('services.admin_services._check_role', return_value = 0)
    def test_delete_user_UNAUTHORIZED(self, patch1):
        session = MockSession.setUp()
        result = delete_user("neshto", "fake_token", session)
        self.assertEqual(result.body, b"You need admin privileges.")
        MockSession.tearDown()


    @patch('services.admin_services._check_role', return_value = 2)
    def test_delete_user_PlayerNonExistent_BADREQUEST(self, patch1):
        session = MockSession.setUp()
        result = delete_user("neshto", "fake_token", session)
        self.assertEqual(result.body, b"No such user.")
        MockSession.tearDown()


    @patch('services.admin_services._check_role', return_value = 2)
    def test_delete_user_SUCCESS(self, patch1):
        session = MockSession.setUp()
        user = register("neshto@gmail.com", "password", session)
        result = delete_user("neshto@gmail.com", "fake_token", session)
        self.assertEqual(type(result), NoContent)
        MockSession.tearDown()


class DeletePlayer_Should(unittest.TestCase):
    @patch('services.admin_services._check_role', return_value = 0)
    def test_delete_player_UNAUTHORIZED(self, patch1):
        session = MockSession.setUp()
        result = delete_player(1, "fake_token", session)
        self.assertEqual(result.body, b"You need admin privileges.")
        MockSession.tearDown()


    @patch('services.admin_services._check_role', return_value = 2)
    def test_delete_player_UNSUCCESSFUL(self, patch1):
        session = MockSession.setUp()
        result = delete_player(1, "fake_token", session)
        self.assertEqual(result.body, b"No such player.")
        MockSession.tearDown()


    @patch('services.players_services._check_role', return_value = 2)
    @patch('services.admin_services._check_role', return_value = 2)
    def test_delete_player_SUCCESS(self, patch1, patch2):
        session = MockSession.setUp()
        player = create_new_profile(player_profile(name = "Namey", country = "Countryy"),"fake token", session)
        result = delete_player(1, "fake_token", session)
        self.assertEqual(type(result), NoContent)
        MockSession.tearDown()


class DeleteMatch_Should(unittest.TestCase):
    @patch('services.admin_services._check_role', return_value = 0)
    def test_delete_match_UNAUTHORIZED(self, patch1):
        session = MockSession.setUp()
        result = delete_match(1, "fake_token", session)
        self.assertEqual(result.body, b"You need admin privileges.")
        MockSession.tearDown()


    @patch('services.admin_services._check_role', return_value = 2)
    def test_delete_match_UNSUCCESSFUL(self, patch1):
        session = MockSession.setUp()
        result = delete_match(1, "fake_token", session)
        self.assertEqual(result.body, b"No such player.")
        MockSession.tearDown()


    # @patch('services.players_services._check_role', return_value = 2)
    # @patch('services.match_services._check_role', return_value = 2)
    # @patch('services.admin_services._check_role', return_value = 2)
    # def test_delete_match_SUCCESS(self, patch1, patch2, patch3):
    #     session = MockSession.setUp()
    #     player = create_new_profile(player_profile(name = "Namey1", country = "Countryy"),"fake token", session)
    #     player = create_new_profile(player_profile(name = "Namey2", country = "Countryy"),"fake token", session)
    #     event = create_match(match(date = "2022-11-20", player_one_id = 1, player_two_id = 2, format = "SL"),"fake token", session)
    #     result = delete_match(1, "fake_token", session)
    #     self.assertEqual(type(result), NoContent)
    #     MockSession.tearDown()


class DeleteTournament_Should(unittest.TestCase):
    @patch('services.admin_services._check_role', return_value = 0)
    def test_delete_tournament_UNAUTHORIZED(self, patch1):
        session = MockSession.setUp()
        result = delete_tournament("tournament", "fake_token", session)
        self.assertEqual(result.body, b"You need admin privileges.")
        MockSession.tearDown()


    @patch('services.admin_services._check_role', return_value = 2)
    def test_delete_tournament_UNSUCCESSFUL(self, patch1):
        session = MockSession.setUp()
        result = delete_tournament("tournament", "fake_token", session)
        self.assertEqual(result.body, b"No such tournament.")
        MockSession.tearDown()

    @patch('services.tournament_services.query_select', return_value = False)
    @patch('services.tournament_services._check_role', return_value = 2)
    @patch('services.admin_services._check_role', return_value = 2)
    def test_delete_tournament_SUCCESS(self, patch1, patch2, patch3):
        session = MockSession.setUp()
        event = create_tournament(tournament_creation(
            title = "Tourny",
            format = "knockout",
            participants = ["Nemo", "Alice"],
            match_format = "SL",
            prize = "None"
        ),"fake token", session)
        result = delete_tournament("Tourny", "fake_token", session)
        self.assertEqual(type(result), NoContent)
        MockSession.tearDown()
