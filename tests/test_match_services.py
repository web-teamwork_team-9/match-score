from sqlmodel import Session, SQLModel, create_engine
from services.match_services import *
from services.tournament_services import create_tournament
from services.players_services import *
from services.match_services import _update_knockout_match, _update_league_match
from data.data_models import *
from common.responses import *
from unittest.mock import patch
from os import path, remove
from datetime import date
import unittest


class MockSession:
    @classmethod
    def setUp(self):
        self.flag = True
        self.DB_ENGINE = create_engine("sqlite:///testing.db", connect_args={"check_same_thread": False})
        self.SESSION = Session(self.DB_ENGINE)
        SQLModel.metadata.create_all(self.DB_ENGINE)
        return self.SESSION


    @classmethod
    def tearDown(self):
        self.flag = True
        self.SESSION.close()
        if path.exists("testing.db"):
            remove("testing.db")


class CreateMatch_Should(unittest.TestCase):
    def test_create_UNATHORIZED(self):
        with patch('services.match_services._check_role', return_value = 0):
            session = MockSession.setUp()
            result = create_match(match(
                match_id = 1,
                date =  date.today(),
                score_player_one = 0,
                score_player_two = 0,
                winner = 0,
                duration = "22:07",
                tournament_id = 1,
                player_one_id = 5,
                player_two_id = 6), "fake-token", session)
            
        self.assertEqual(result.body, b"You need to be an admin or director.")
        MockSession.tearDown()
    

    def test_create_bad_date(self): 
        with patch('services.match_services._check_role', return_value = 1):
            session = MockSession.setUp()
            result = create_match(match(
                match_id = 1,
                date =  date.today() - timedelta(days = 1),
                score_player_one = 0,
                score_player_two = 0,
                winner = 0,
                duration = "22:07",
                tournament_id = 1,
                player_one_id = 5,
                player_two_id = 6), "fake-token", session)
            
        self.assertEqual(result.body, b"You cannot create a match to a previous date.")
        MockSession.tearDown()


    @patch('services.match_services.query_select', return_value = False)
    def test_create_same_profiles(self, patch1):
        with patch('services.match_services._check_role', return_value = 1):
            session = MockSession.setUp()
            result = create_match(match(
                match_id = 1,
                date =  date.today(),
                score_player_one = 0,
                score_player_two = 0,
                winner = 0,
                duration = "22:07",
                tournament_id = 1,
                player_one_id = 5,
                player_two_id = 5), "fake-token", session)
            
        self.assertEqual(result.body, b"Player ids' should not be the same.")
        MockSession.tearDown()
    

    @patch('services.match_services.query_select', return_value = False)
    def test_create_profile_doesnt_exist(self, patch1):
        with patch('services.match_services._check_role', return_value = 1):
            session = MockSession.setUp()
            result = create_match(match(
                match_id = 1,
                date =  date.today(),
                score_player_one = 0,
                score_player_two = 0,
                winner = 0,
                duration = "22:07",
                tournament_id = 1,
                player_one_id = 5,
                player_two_id = 6), "fake-token", session)
            
        self.assertEqual(result.body, b"Player with id: 5 does not exist.")
        MockSession.tearDown()

    
    @patch('services.match_services.query_select', return_value = True)
    def test_create_SUCCESS(self, patch1):
        with patch('services.match_services._check_role', return_value = 1):
            session = MockSession.setUp()
            result = create_match(match(
                match_id = 1,
                date =  date.today(),
                score_player_one = 0,
                score_player_two = 0,
                winner = 0,
                duration = "22:07",
                tournament_id = 1,
                player_one_id = 5,
                player_two_id = 6,
                format = "TL"), "fake-token", session)
                         
        self.assertEqual(type(result), NoContent)
        MockSession.tearDown()


class CreateTournamentMatches_Should(unittest.TestCase):
    @patch('services.tournament_services._check_role', return_value = 2)
    @patch('services.tournament_services.query_select', return_value = False)
    def test_create_tournament_matches_SUCCESS(self, patch1, patch2):
        session = MockSession.setUp()
        create_tournament(tournament_creation(
            title = "Nameyy",
            format = "knockout",
            participants = ["Leonardo", "Kenya"],
            match_format = "TL",
            prize = "None"), "fake-token", session)
        
        result = create_tournament_matches(
            ["Leonardo", "Kenya"], 
            query_select(tournament, tournament.title, "Nameyy", session), 
            session)
        
        self.assertIsInstance(result, list)
        self.assertTrue(result)
        MockSession.tearDown()


class GetAll_Should(unittest.TestCase):
    def test_get_all_UNSUCCESSFUL(self):
        session = MockSession.setUp()
        result = get_all(session)
        self.assertEqual(result.body, b"No matches found.")
        MockSession.tearDown()

    @patch('services.match_services._check_role', return_value = 2)
    @patch('services.match_services.query_select', return_value = True)
    def test_get_all_SUCCESSFUL(self, patch1, patch2):
        session = MockSession.setUp()
        result = create_match(match(
                match_id = 13,
                date =  date.today(),
                score_player_one = 0,
                score_player_two = 0,
                winner = 0,
                duration = "22:07",
                tournament_id = 1,
                player_one_id = 5,
                player_two_id = 6,
                format = "TL"), "fake-token", session)

        self.assertEqual(type(get_all(session)), list)
        MockSession.tearDown()


class GetEventID_Should(unittest.TestCase):
    def test_get_match_UNSUCCESSFUL(self):
        session = MockSession.setUp()
        result = get_event_id(1, session)
        self.assertEqual(result.body, b"No such match.")
        MockSession.tearDown()

    @patch('services.match_services._check_role', return_value = 2)
    def test_get_id_SUCCESSFUL(self, patch1):
        session = MockSession.setUp()
        with patch('services.match_services.query_select', return_value = True):
            create_match(match(
                match_id = 10,
                date =  date.today(),
                score_player_one = 0,
                score_player_two = 10,
                winner = 0,
                duration = "22:07",
                tournament_id = 1,
                player_one_id = 5,
                player_two_id = 6,
                format = "TL"), "fake-token", session)

        self.assertEqual(type(get_event_id(10, session)), dict)
        MockSession.tearDown()


class UpdateNormalMatch_Should(unittest.TestCase):
    def test_update_UNATHORIZED(self):
        with patch('services.match_services._check_role', return_value = 0):
            session = MockSession.setUp()

            score = match_score(
                score_player_one = 5,
                score_player_two = 6,
                duration = "22:05")

            result = update_normal_match(11, score, "fake-token", session)
            
        self.assertEqual(result.body, b"You need to be an admin or director.")
        MockSession.tearDown()


    @patch('services.players_services._check_role', return_value = 1)
    @patch('services.players_services._is_authenticated', return_value = 123)
    @patch('services.match_services._is_authenticated', return_value = 123)
    @patch('services.match_services._check_role', return_value = 1)
    def test_update_match_fail_has_winner(self, patch1, patch2, patch3, patch4):    
        session = MockSession.setUp()
        create_new_profile(player_profile(profile_id = 155, name = "Leo Meo Messi", country = "Oceania"), "fake-token", session)
        create_new_profile(player_profile(profile_id = 166, name = "Leo Teo Messi", country = "Oceania"), "fake-token", session)

        create_match(match(
            match_id = 15,
            date =  date.today(),
            score_player_one = 0,
            score_player_two = 0,
            winner = 1,
            duration = "22:07",
            tournament_id = 1,
            player_one_id = 155,
            player_two_id = 166,
            format = "TL"), "fake-token", session)

        score = match_score(
            score_player_one = 5,
            score_player_two = 6,
            duration = "22:05")

        result = update_normal_match(15, score, "fake-token", session)
        
        self.assertEqual(result.body, b"This match is already finished.")
        MockSession.tearDown()


    @patch('services.match_services._update_profile_ratio_statistics', return_value = True)
    @patch('services.players_services._check_role', return_value = 1)
    @patch('services.players_services._is_authenticated', return_value = 123)
    @patch('services.match_services._is_authenticated', return_value = 123)
    @patch('services.match_services._check_role', return_value = 1)
    def test_update_match_SUCCESS(self, patch1, patch2, patch3, patch4, patch5):    
        session = MockSession.setUp()
        create_new_profile(player_profile(profile_id = 155, name = "Leo Meo Messi", country = "Oceania"), "fake-token", session)
        create_new_profile(player_profile(profile_id = 166, name = "Leo Teo Messi", country = "Oceania"), "fake-token", session)

        create_match(match(
            match_id = 15,
            date =  date.today(),
            score_player_one = 0,
            score_player_two = 0,
            winner =  None,
            duration = "22:07",
            tournament_id = None,
            player_one_id = 155,
            player_two_id = 166,
            format = "TL"), "fake-token", session)

        score = match_score(
            score_player_one = 5,
            score_player_two = 6,
            duration = "22:05")

        result = update_normal_match(15, score, "fake-token", session)
        
        self.assertEqual(type(result), match)
        MockSession.tearDown()


class UpdateKnockoutMatch_Should(unittest.TestCase):
    @patch('services.match_services._update_profile_ratio_statistics', return_value = True)
    @patch('services.tournament_services._check_role', return_value = 1)
    @patch('services.match_services._check_role', return_value = 1)
    def test_update_knockout_match_cannot_be_draw(self, patch1, patch2, patch3):    
        session = MockSession.setUp()

        create_tournament(tournament_creation(
            title = "Nameyy",
            format = "knockout",
            participants = ["Leonardo", "Penka"],
            match_format = "TL",
            prize = "None"), "fake-token", session)

        create_match(match(
            match_id = 15,
            date =  date.today(),
            score_player_one = 0,
            score_player_two = 0,
            winner =  None,
            duration = "22:07",
            tournament_id = None,
            player_one_id = 1,
            player_two_id = 2,
            format = "TL"), "fake-token", session)

        tourn = query_select(tournament, tournament.title, "Nameyy", session)
        ran_match = query_select(match, match.match_id, 15, session)

        result = _update_knockout_match(tourn, ran_match, session)
        self.assertEqual(result.body, b"A knockout match cannot be a draw!")
        MockSession.tearDown()


    @patch('services.match_services.queries_select', return_value = [])
    @patch('services.match_services._update_profile_ratio_statistics', return_value = True)
    @patch('services.tournament_services._check_role', return_value = 1)
    @patch('services.match_services._check_role', return_value = 1)
    def test_update_knockout_match_no_part_error(self, patch1, patch2, patch3, patch4):    
        session = MockSession.setUp()

        create_tournament(tournament_creation(
            title = "Nameyy",
            format = "knockout",
            participants = ["Leonardo", "Penka"],
            match_format = "TL",
            prize = "None"), "fake-token", session)

        create_match(match(
            match_id = 15,
            date =  date.today(),
            score_player_one = 5,
            score_player_two = 0,
            winner =  1,
            duration = "22:07",
            tournament_id = None,
            player_one_id = 1,
            player_two_id = 2,
            format = "TL"), "fake-token", session)

        tourn = query_select(tournament, tournament.title, "Nameyy", session)
        ran_match = query_select(match, match.match_id, 15, session)
        
        result = _update_knockout_match(tourn, ran_match, session)
        self.assertEqual(result.body, b"Ooops, something went wrong...")
        MockSession.tearDown()


    @patch('services.match_services._update_profile_ratio_statistics', return_value = True)
    @patch('services.tournament_services._check_role', return_value = 1)
    @patch('services.match_services._check_role', return_value = 1)
    def test_update_knockout_match_SUCCESS(self, patch1, patch2, patch3):    
        session = MockSession.setUp()

        create_tournament(tournament_creation(
            title = "Nameyy",
            format = "knockout",
            participants = ["Leonardo", "Penka"],
            match_format = "TL",
            prize = "None"), "fake-token", session)

        create_match(match(
            match_id = 15,
            date =  date.today(),
            score_player_one = 5,
            score_player_two = 0,
            winner =  1,
            duration = "22:07",
            tournament_id = None,
            player_one_id = 1,
            player_two_id = 2,
            format = "TL"), "fake-token", session)

        tourn = query_select(tournament, tournament.title, "Nameyy", session)
        ran_match = query_select(match, match.match_id, 15, session)
        
        result = _update_knockout_match(tourn, ran_match, session)
        self.assertEqual(result.body, b"Match updated successfully.")
        MockSession.tearDown()


class UpdateLeagueMatch_Should(unittest.TestCase):
    @patch('services.match_services._update_profile_ratio_statistics', return_value = True)
    @patch('services.tournament_services._check_role', return_value = 1)
    @patch('services.match_services._check_role', return_value = 1)
    def test_update_league_match_no_winner(self, patch1, patch2, patch3):    
        session = MockSession.setUp()

        create_tournament(tournament_creation(
            title = "Nameyy",
            format = "league",
            participants = ["Leonardo", "Penka", "Stoqn"],
            match_format = "TL",
            prize = "None"), "fake-token", session)

        create_match(match(
            match_id = 15,
            date =  date.today(),
            score_player_one = 5,
            score_player_two = 0,
            winner =  None,
            duration = "22:07",
            tournament_id = None,
            player_one_id = 1,
            player_two_id = 2,
            format = "TL"), "fake-token", session)

        tourn = query_select(tournament, tournament.title, "Nameyy", session)
        ran_match = query_select(match, match.match_id, 15, session)
        
        result = _update_league_match(tourn, ran_match, session)
        self.assertEqual(result.body, b"Ooops, something went wrong...")
        MockSession.tearDown()


    @patch('services.match_services._update_profile_ratio_statistics', return_value = True)
    @patch('services.tournament_services._check_role', return_value = 1)
    @patch('services.match_services._check_role', return_value = 1)
    def test_update_league_match_SUCCESS(self, patch1, patch2, patch3):    
        session = MockSession.setUp()

        create_tournament(tournament_creation(
            title = "Nameyy",
            format = "league",
            participants = ["Leonardo", "Penka", "Stoqn"],
            match_format = "TL",
            prize = "None"), "fake-token", session)

        create_match(match(
            match_id = 15,
            date =  date.today(),
            score_player_one = 5,
            score_player_two = 0,
            winner =  1,
            duration = "22:07",
            tournament_id = None,
            player_one_id = 1,
            player_two_id = 2,
            format = "TL"), "fake-token", session)

        tourn = query_select(tournament, tournament.title, "Nameyy", session)
        ran_match = query_select(match, match.match_id, 15, session)
        
        result = _update_league_match(tourn, ran_match, session)
        self.assertEqual(result.body, b"Match updated successfully.")
        MockSession.tearDown()
