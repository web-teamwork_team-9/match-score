from sqlmodel import Session, SQLModel, create_engine
from services.players_services import *
from services.players_services import _update_profile_ratio_statistics
from data.data_models import *
from common.responses import *
from unittest.mock import patch
from os import path, remove
import unittest


class MockSession:
    @classmethod
    def setUp(self):
        self.flag = True
        self.DB_ENGINE = create_engine("sqlite:///testing.db", connect_args={"check_same_thread": False})
        self.SESSION = Session(self.DB_ENGINE)
        SQLModel.metadata.create_all(self.DB_ENGINE)
        return self.SESSION


    @classmethod
    def tearDown(self):
        self.flag = True
        self.SESSION.close()
        if path.exists("testing.db"):
            remove("testing.db")


class NewProfile_Should(unittest.TestCase):
    @patch('services.players_services._check_role', return_value = 0)
    def test_new_profile_UNAUTHORIZED(self, patch1):
        session = MockSession.setUp()
        result = create_new_profile(player_profile(name = "Leonardo Messi", country = "Oceania"), "fake-token", session)
        self.assertEqual(result.body, b"You need to be an admin or director.")
        MockSession.tearDown()


    # @patch('services.players_services._check_role', return_value = 2)
    # def test_new_profile_BADREQUEST(self, patch1):
    #     session = MockSession.setUp()
    #     with patch('services.players_services.query_upsert', return_value = ValueError()):
    #         result = create_new_profile(player_profile(name = "Leonardo Messi", country = "Oceania"), "fake-token", session)
    #         self.assertEqual(result.body, b"Ooops, something went wrong...")
    #     MockSession.tearDown()


    @patch('services.players_services._check_role', return_value = 2)
    def test_new_profile_SUCCESSFUL(self, patch1):
        session = MockSession.setUp()
        result = create_new_profile(player_profile(name = "Leonardo Messi", country = "Oceania"), "fake-token", session)
        self.assertEqual(type(result), NoContent)
        MockSession.tearDown()


class UpdateProfile_Should(unittest.TestCase):
    @patch('services.players_services._check_role', return_value = 2)
    def test_update_noprofile_BADREQUEST(self, patch1):
        session = MockSession.setUp()
        with patch('services.players_services.query_upsert', return_value = None):
            result = update_profile(1, profile_updates(name = "Leonardo Messi", country = "Oceania"), "fake-token", session)
        self.assertEqual(result.body, b"No such player.")
        MockSession.tearDown()


    @patch('services.players_services._check_role', return_value = 0)
    def test_update_isstandard_UNAUTHORIZED(self, patch1):
        session = MockSession.setUp()
        with patch('services.players_services._check_role', return_value = 2):
            profile = create_new_profile(player_profile(name = "Leonardo Messi", country = "Oceania", user_id = "1"), "fake-token", session)
        result = update_profile(1, profile_updates(name = "Leonardo Messi", country = "Oceania"), "fake-token", session)
        self.assertEqual(type(result), Unauthorized)
        MockSession.tearDown()


    @patch('services.players_services._check_role', return_value = 1)
    def test_update_hasowner_isdirector_UNSUCCESSFUL(self, patch1):
        session = MockSession.setUp()
        profile = create_new_profile(player_profile(name = "Leonardo Messi", country = "Oceania", user_id = "1"), "fake-token", session)
        result = update_profile(1, profile_updates(country = "Australia"), "fake-token", session)
        self.assertEqual(type(result), Unauthorized)
        MockSession.tearDown()


    @patch('services.players_services._is_authenticated', return_value = "1")
    @patch('services.players_services._check_role', return_value = 0)
    def test_update_hasowner_isowner_SUCCESSFUL(self, patch1, patch2):
        session = MockSession.setUp()
        with patch('services.players_services._check_role', return_value = 2):
            profile = create_new_profile(player_profile(name = "Leonardo Messi", country = "Oceania", user_id = "1"), "fake-token", session)
        result = update_profile(1, profile_updates(country = "Oceania"), "fake-token", session)
        self.assertEqual(type(result), NoContent)
        MockSession.tearDown()


    @patch('services.players_services._check_role', return_value = 1)
    def test_update_noowner_isdirector_SUCCESSFUL(self, patch1):
        session = MockSession.setUp()
        profile = create_new_profile(player_profile(name = "Leonardo Messi", country = "Oceania"), "fake-token", session)
        result = update_profile(1, profile_updates(country = "Australia"), "fake-token", session)
        self.assertEqual(type(result), NoContent)
        MockSession.tearDown()


    @patch('services.players_services._check_role', return_value = 2)
    def test_update_noowner_isadmin_SUCCESSFUL(self, patch1):
        session = MockSession.setUp()
        profile = create_new_profile(player_profile(name = "Leonardo Messi", country = "Oceania"), "fake-token", session)
        result = update_profile(1, profile_updates(name = "Leonardo Messi", country = "Oceania"), "fake-token", session)
        self.assertEqual(type(result), NoContent)
        MockSession.tearDown()


    @patch('services.players_services._check_role', return_value = 2)
    def test_update_hasowner_isadmin_SUCCESSFUL(self, patch1):
        session = MockSession.setUp()
        profile = create_new_profile(player_profile(name = "Leonardo Messi", country = "Oceania", user_id = "1"), "fake-token", session)
        result = update_profile(1, profile_updates(name = "Leonardo Messi", country = "Oceania"), "fake-token", session)
        self.assertEqual(type(result), NoContent)
        MockSession.tearDown()


    # @patch('services.players_services._check_role', return_value = 2)
    # def test_update_query_BADREQUEST(self, patch1):
    #     session = MockSession.setUp()
    #     create_new_profile(player_profile(name = "Leonardo Messi", country = "Oceania", user_id = "1"), "fake-token", session)
    #     with patch('services.players_services.query_upsert', return_value = ValueError()):
    #         result = update_profile(1, profile_updates(name = "Leonardo Messi", country = "Oceania"), "fake-token", session)
    #     self.assertEqual(result.body, b"Ooops, something went wrong...")
    #     MockSession.tearDown()


class ViewProfile_Should(unittest.TestCase):
    def test_new_profile_SUCCESSFUL(self):
        session = MockSession.setUp()
        with patch('services.players_services._check_role', return_value = 2):
            profile = create_new_profile(player_profile(name = "Leonardo Messi", country = "Oceania"), "fake-token", session)
        result = view_profile(1, session)
        self.assertEqual(type(result), dict)
        MockSession.tearDown()


    def test_new_profile_UNSUCCESSFUL(self):
        session = MockSession.setUp()
        result = view_profile(1, session)
        self.assertEqual(type(result), BadRequest)
        MockSession.tearDown()


class UpdateProfileRatioStatistics_Should(unittest.TestCase):
    @patch('services.players_services.query_select', return_value = ValueError())
    def test_update_statistics_BADREQUEST(self, patch1):
        session = MockSession.setUp()
        result = _update_profile_ratio_statistics(match, session)
        self.assertEqual(result.body, b"Ooops, something went wrong...")
        MockSession.tearDown()
